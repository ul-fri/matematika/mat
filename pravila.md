# Obveznosti študenta

Študent izpolni svoje obveznosti pri predmetu, če:

  * obiskuje predavanja in vaje,
  * uspešno opravi sprotne obveznosti in
  * uspešno opravi izpit.

## Obisk predavanj in vaj

Delo pri predmetu bo potekalo v tedenskem ritmu. Vsak teden imamo na sporedu

  * 3 ure predavanj in
  * 2 uri vaj.

Obisk predavanj in vaj je obvezen.

## Sprotne obveznosti

### Domače naloge
Praviloma bo vsak teden na voljo domača naloga. Da jo uspešno opravite, morate pri njej zbrati vsaj 50% možnih točk.

Za uspešno opravljene domače naloge mora študent opraviti vse domače naloge razen največ dveh.

### Kolokviji
 Znanje bomo sproti preverjali na 90-minutnih računskih testih (kolokvijih),
na katerih bodo študentje tipično reševali računske naloge.
Na vsakem kolokviju si lahko pomagate z enim A4 listom z izpiski.

Na kolokvije se ni potrebno prijavljati, razporeditev študentov po učilnicah bo objavljena pred kolokvijem na spletni učilnici predmeta.

V kolikor študent doseže v povprečju obeh kolokvijev vsaj 50%, se mu s tem prizna računski izpit pri predmetu.

## Izpit

Izpit je pisni in sestavljen iz računskega in teoretičnega dela.
Izpitni roki so zabeleženi v sistemu Studis in nanje se je potrebno pravočasno prijaviti.
Če se želite od izpita odjaviti, lahko to storite preko Studisa ali osebno pri izvajalcu predmeta pred začetkom izpita. Po začetku izpita odjava ni več mogoča.

Pogoj za pristop k izpitu so uspešno opravljene sprotne obveznosti.

Na dan izpita bo najprej na sporedu računski, nato teoretični del.
V kolikor ste računski del izpita v tem študijskem letu že opravili, ga lahko izpustite.

### Računski izpit

Računski izpit traja 90 minut. Na njem je mogoče doseči 100 točk, za pozitivno oceno zadošča 50 točk.
Na računskem izpitu si lahko pomagate z dvema A4 listoma z izpiski.

Računski izpit lahko opravite tudi s kolokviji.
V tem primeru je rezultat računskega izpita enak povprečju rezultatov kolokvijev. 

### Teoretični izpit  

Teoretični izpit traja 45 minut. Na njem je mogoče doseči 100 točk, za pozitivno oceno zadošča 50 točk.
Na teoretičnem izpitu ni dovoljena uporaba literature (tudi ne listov s formulami), kalkulatorja in drugih elektronskih naprav. 

Nanj lahko pristopite le, če imate opravljen računski izpit oz. ste ga opravljali na tem izpitnem roku.



## Ocena pri predmetu

Za pozitivno opravljen predmet morate imeti pozitivno oceno domačih nalog, računskega izpita in teoretičnega izpita.

Študent pri predmetu dobi eno samo oceno, ki se določi na osnovi
rezultata pri računskem in teoretičnem delu izpita.
