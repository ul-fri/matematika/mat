# Matematika na VSP FRI

Načrt predmeta Matematika na visokem strokovnem programu na Fakulteti za 
računalništvo in informatiko.

Ta projekt je namenjen zbranemu gradivu pri predmetu Matematika.

## Učni načrt

 * Cilji predmeta (obseg snovi, kompetence)
 * povezave z drugimi predmeti
 * [Kompetence](kompetence.md)
 * učne poti
 * potrebno predznanje

## Izvedba

 * predavanja
 * vaje
 * dejavnosti študentov
 * prverjanje znanja

## Podporna infrastruktura

 * Gradiva: Učbenik, zbirka nalog
 * spletna učilnica
 * povezane vsebine na internetu
 * Spletna stran
 * Repozitorij z aktivnostmi

