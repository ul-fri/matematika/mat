# Kompetence pri predmetu Matematika

V tem dokumentu so podrobno opisane kompetence, ki jih študentje lahko pridobijo, 
če sodelujejo pri tem predmetu.

## Povezave

 * [Kompetence v moodlu](https://docs.moodle.org/36/en/Competencies)
 * [Učne poti v moodlu](https://docs.moodle.org/36/en/Learning_plans)